<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2017/03/06
 * 
 *
 */
namespace Admin\Controller;
use Think\Auth;
//后台管理员
class AdminController extends CommonController {
	
	//用户列表
     public function admin_list(){
    	$m = M('Admin');
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$data = $m->order('id DESC')->page($nowPage.','.PAGE_SIZE)->select();
    	$auth = new Auth();
    	foreach ($data as $k=>$v){
    		$group = $auth->getGroups($v['id']);
    		$data[$k]['group'] = $group[0]['title'];
    	}
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('data',$data);
    	$this->display('admin_list');
    }
	
    //检查账号是否已注册
    public function check_account(){
    	$m = M('admin');
    	$where['account'] = I('account');	//账号
    	$data = $m->field('id')->where($where)->find();
    	if(empty($data)){
    		$this->ajaxReturn(0);   //不存在
    	}else{
    		$this->ajaxReturn(1);	//存在
    	}
    }
        
    //添加用户
    public function admin_add(){
    	if(!empty($_POST)){
    		$m = M('admin');
    		$data['account'] = I('account');
    		$data['password'] = md5(I('password'));
    		$data['create_time'] = time();		//创建时间
    		$where['account'] = I('account');
    		$result = $m->where($where)->find();
    		if(!empty($result)){
    			$this->ajaxReturn(0);  //用户名重复
    		}
    		//添加用户
    		$result['uid']  = $m->add($data);
    		$result['group_id'] = I('group_id');	//用户组ID
    		if($result['uid']){
    			$m = M('auth_group_access');
    			//分配用户组
    			if($m->add($result)){
    				$this->ajaxReturn(1);	//分配用户组成功
    			}else{
    				$this->ajaxReturn(2);	//分配用户组失败
    			}
    		}else{
    			$this->ajaxReturn(0);  //添加用户失败
    		}
    	}else{
    		$m = M('auth_group');  //查询用户组
    		$data = $m->field('id,title')->select();
    		$this->assign('data',$data);
    		$this->display('admin_add');
    	}
    }

    //编辑
    public function admin_edit(){
    	if(!empty($_POST)){
    		//修改所属组
    		$access = M('auth_group_access');
    		if (empty($_POST['group_id'])){
    			$this->error('请选择用户组');
    		}
    		$result = $access->where('uid='.$_POST['id'])->find();
    		if(empty($result)){
    			$map['uid'] = $_POST['id'];
    			$map['group_id'] = $_POST['group_id'];
    			$access->add($map);
    		}else {
    			$save['group_id'] = $_POST['group_id'];
    			$access->where('uid='.$_POST['id'])->save($save);
    		}
    		$data['id'] = $_POST['id'];
    		if(!empty($_POST['password'])){
    			$data['password'] = md5($_POST['password']);
    		}
    		if ($_POST['status'] >= 0){
    			$data['status'] = $_POST['status'];
    		}
    		$m = M('admin');
    		$result = $m->where('id='.$data['id'])->save($data);
    		if ($result === false){
                $this->ajaxReturn(2);   //分配用户组成功
    			$this->error('修改失败');
    		}else{
                $this->ajaxReturn(1);   //分配用户组成功
    			$this->success('修改成功');
    		}
    	}else{
    		$m = M('admin');
    		$result = $m->where('id='.I('id'))->find();   		
    		//获取当前所属组
    		$auth = new Auth();
    		$group = $auth->getGroups($result['id']);
    		$result['title'] = $group[0]['title'];
    		$result['group_id'] = $group[0]['group_id'];
    		$this->assign('vo',$result);
    		//获取所有组
    		$m = M('auth_group');
    		$group = $m->order('id DESC')->select();
    		$this->assign('group',$group);
    		$this->display('admin_edit');
    	}
    }
    
    //删除用户
    public function admin_del(){
     	$id = $_POST['id'];		//用户ID
    	if($id == 1){
    		$this->ajaxReturn(0);	//不允许删除超级管理员
    	}
    	$m = M('auth_group_access');    	
    	$m->where('uid='.$id)->delete();   //删除权限表里面给的权限
    	    	
    	$m = M('admin');
    	$result = $m->where('id='.$id)->delete();
    	if ($result){
    		$this->ajaxReturn(1);	//成功
    	}else {
    		$this->ajaxReturn(0);	//删除失败
    	}
    }

    //角色-组
    public function auth_group(){
    	$m = M('auth_group');
    	$data = $m->order('id DESC')->select();
 
        $nav = C('CHECK_TITLE');
        foreach ($nav as $key => $value) {
            $list[$value] = M('AdminRole')->where(array('type'=>$key))->order('type asc,order_number ASC')->select();
        }
        // print_r($list);
        $this->assign('list',$list);
    	$this->assign('data',$data);
    	$this->display();
    }
    
    //添加组
    public function group_add(){
    	if(!empty($_POST)){
    		$data['rules'] = I('rule_ids');
    		if(empty($data['rules'])){
    			$this->error('权限不能为空');
    		}
            // $data['rolepost'] = I('rolepost');
    		$m = M('auth_group');
    		$data['title'] = I('title');
            // $data['rolepost'] = implode(',', $data['rolepost']);
    		$data['rules'] = implode(',', $data['rules']);
    		$data['create_time'] = time();
    		if($m->add($data)){
    			$this->success('添加成功',U('Admin/auth_group'));
    		}else{
    			$this->error('添加失败');
    		}
    	}else{

            $currentPost = C('current_post');
            // 获取用户组数据
            $where['pid']=0;
            $group_data=M('Auth_group')->where($where)->find();
            $group_data['rules']=explode(',', $group_data['rules']);
            // 获取规则数据
            $rule_data=D('AuthRule')->getTreeData('level','id','title');
            $assign=array(
                'group_data'=>$group_data,
                'rule_data'=>$rule_data,
                'currentPost' => $currentPost
                );
	    	$this->assign($assign);	// 顶级
    		$this->display('group_add');
    	}	
    }

  //*****************权限-用户组*****************
    /**
     * 分配权限
     */
    public function group_edit(){
    	$m = M('auth_group');
    	if(!empty($_POST)){
    		$data['id'] = I('id');
    		$data['title'] = I('title');
            // $data['rolepost'] = implode(',', I('rolepost'));
    		$data['rules'] = implode(',', I('rule_ids'));
    		if($m->save($data)){
    			$this->success('修改成功');
    		}else{
    			$this->error('修改失败');
    		}
    	}else{
    		$where['id'] = I('id');	//组ID
    		$reuslt = $m->field('id,title,rules')->where($where)->find();
    		$reuslt['rules'] = ','.$reuslt['rules'].',';
    		$this->assign('reuslt',$reuslt); 
            // 获取用户组数据
            $group_data=M('Auth_group')->where($where)->find();
            $group_data['rules']=explode(',', $group_data['rules']);
            $group_data['rolepost']=explode(',', $group_data['rolepost']);
            // 获取规则数据
            $rule_data=D('AuthRule')->getTreeData('level','id','title');
            $currentPost = C('current_post');
            $assign=array(
                'group_data'=>$group_data,
                'rule_data'=>$rule_data,
                'currentPost' => $currentPost
                );
            // print_r($group_data);exit;
            $this->assign($assign);
            $this->display('group_edit');



    	}
    }


    //删除组
    public function group_del(){
    	$where['id'] = I('id');
    	$m = M('auth_group');
    	if($m->where($where)->delete()){
    		$this->ajaxReturn(1);
    	}else{
    		$this->ajaxReturn(0);
    	}
    }


    //******************权限***********************
    /**
     * 权限列表
     */
    public function auth_rule(){
        $data=D('AuthRule')->getTreeData('tree','id','title');
        $assign=array(
            'data'=>$data
            );
        $this->assign($assign);
        $this->display('auth_rule');
    }

    /**
     * 添加权限
     */
    public function auth_rule_add(){
        $data=I('post.');
        // unset($data['id']);
        $result=D('AuthRule')->addData($data);
        if ($result) {
            $this->success('添加成功',U('Admin/Admin/auth_rule'));
        }else{
            $this->error('添加失败');
        }
    }

    /**
     * 修改权限
     */
    public function auth_rule_edit(){
        $data=I('post.');
        $map=array(
            'id'=>$data['id']
            );
        $result=D('AuthRule')->editData($map,$data);
        if ($result) {
            $this->success('修改成功',U('Admin/Admin/auth_rule'));
        }else{
            $this->error('修改失败');
        }
    }

    /**
     * 删除权限
     */
    public function auth_rule_delete(){
        $id=I('get.id');
        $map=array(
            'id'=>$id
            );
        $result=D('AuthRule')->deleteData($map);
        if($result){
            $this->success('删除成功',U('Admin/Admin/auth_rule'));
        }else{
            $this->error('请先删除子权限');
        }

    }

    //修改用户资料
    public function user(){
        if(!empty($_POST)){
            $data['id'] = $_POST['id'];
            if(!empty($_POST['account'])){
                $data['account'] = $_POST['account'];
            }
            if(!empty($_POST['password']) && $_POST['password']!='123456'){
                $data['password'] = md5($_POST['password']);
            }
            $data['mobile'] = $_POST['mobile'];
            $data['email'] = $_POST['email'];
            $m = M('admin');
            $result = $m->where('id='.$data['id'])->save($data);
            if ($result === false){
                $this->error('修改失败');
            }else{
                $this->success('修改成功');
            }
        }else{
            $m = M('admin');
            $result = $m->where('id='.session('aid'))->find();         
            $this->assign('vo',$result);
            $this->display('user');
        }
    }

    /**
     * 获取角色的审批岗位
     * @return [type] [description]
     */
    public function getGroupRole(){
       $group_id = I('get.group_id');
       $list = M('AuthGroupRole')->where(array('group_id'=>$group_id))->field('role_id')->select();
       // print_r($list);exit;
        $data = array(
            'status' =>1,
            'data' =>$list
         );
        $this->ajaxReturn($data);
    }


    /**
     * 添加审批岗位
     */
    public function add_role(){
        $data=I('post.');
        if(!empty($data['role_id'])){
            $info = M('AuthGroupRole')->where(array('group_id'=>$data['group_id']))->field('role_id')->select();
            foreach ($info as $key => $value) {
               $role[] = $value['role_id'];
            }
            if(!empty($info)){
                $diff1 =  array_diff($data['role_id'],$role);
                $diff2 =  array_diff($role,$data['role_id']);
                $list = array_merge($diff1,$diff2);
                if(!empty($list)){
                    foreach ($list as $key => $value) {
                        $num = M('AuthGroupRole')->where(array('group_id'=>$data['group_id'],'role_id'=>$value))->count();
                        if($num>0){
                           $result = M('AuthGroupRole')->where(array('group_id'=>$data['group_id'],'role_id'=>$value))->delete();
                        }else{
                          $map = array(
                               'role_id' =>$value,
                               'group_id' =>$data['group_id']
                            );
                           $result = M('AuthGroupRole')->add($map);
                        }
                    }
                }                
            }else{
                foreach ($data['role_id'] as $key => $value) {
                    $map = array(
                           'role_id' =>$value,
                           'group_id' =>$data['group_id']
                        );
                    $result = M('AuthGroupRole')->add($map);
                }                
            }

        }
        if ($result) {
            $this->success('添加成功',U('Admin/auth_group'));
        }else{
            $this->error('添加失败');
        }
    }


     

}




