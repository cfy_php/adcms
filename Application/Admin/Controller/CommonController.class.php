<?php

/**
 * 后台公共文件 
 * @file   Common.php  
 * @date   2016-8-24 18:28:34 
 * @author Zhenxun Du<5552123@qq.com>  
 * @version    SVN:$Id:$ 
 */
namespace Admin\Controller;  
use Think\Controller;  
use Think\Auth;  
class CommonController extends Controller{  
    protected function _initialize(){
        //session不存在时，不允许直接访问
        if(!session('aid')){
            $this->error('还没有登录，正在跳转到登录页',U('Login/login'));
        }
        //session存在时，不需要验证的权限
        $not_check = array('Admin/Index/index','Admin/Index/welcome','Admin/Index/logout');
        $rule_name=MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;
        //当前操作的请求                 模块名/方法名  
        if(in_array($rule_name, $not_check)){  
            return true;  
        } 
        $auth=new \Think\Auth();
        if(session('aid') == '1'){
            return true; 
        }else{
           $result=$auth->check($rule_name,session('aid'));
            if(!$result){
                $this->error('您没有权限访问');
            }
        }

    }
}  


