<?php
namespace Admin\Controller;
/**
 * 后台用户岗位管理
 */
class RoleController extends CommonController{
	/**
	 * 菜单列表
	 */
	public function index(){
		$nav = C('CHECK_TITLE');
        $data = M('AdminRole')->order('type asc,order_number ASC')->select();
        foreach ($data as $key => $value) {
        	$data[$key]['type_name'] = $nav[$value['type']];
        }
		$assign=array(
			'data'=>$data
			);

		$this->assign('parent',$nav);
		$this->assign($assign);
		$this->display('index');
	}

	/**
	 * 添加菜单
	 */
	public function add(){
		$data=I('post.');
		$result=M('AdminRole')->add($data);
		if ($result) {
			$this->success('添加成功',U('Admin/Role/index'));
		}else{
			$this->error('添加失败');
		}
	}

	/**
	 * 修改菜单
	 */
	public function edit(){
		$data=I('post.');
		$map=array(
			'id'=>$data['id']
			);
		$result=M('AdminRole')->where($map)->save($data);
		if ($result) {
			$this->success('修改成功',U('Admin/Role/index'));
		}else{
			$this->error('修改失败');
		}
	}

	/**
	 * 删除菜单
	 */
	public function delete(){
		$id=I('get.id');
		$map=array(
			'id'=>$id
			);
		$result=M('AdminRole')->where($map)->delete();
		if($result){
			$this->success('删除成功',U('Admin/Role/index'));
		}else{
			$this->error('删除失败');
		}
	}

	/**
	 * 菜单排序
	 */
	public function order(){
		$data=I('post.');
		// print_r(I('post.'));exit;
		$result=D('AdminRole')->orderData($data);
		if ($result) {
			$this->success('排序成功',U('Admin/Role/index'));
		}else{
			$this->error('排序失败');
		}
	}


}
