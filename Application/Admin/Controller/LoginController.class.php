<?php

/**
 *  登陆页
 * @file   Login.php  
 */
namespace Admin\Controller;
use Think\Controller;  
class LoginController extends Controller {
      //ajxa检查验证码
    public function check_code(){
        $code = $_GET['code'];  //验证码
        $verify = new \Think\Verify();
        if($verify->check($code)){
            $this->ajaxReturn(1);   //成功
        }else{
            $this->ajaxReturn(0);   //失败
        }
    }
    
    //登录验证
    public function login(){
        if(!empty($_POST)){
            $map['account'] = I('account');   //用户名
            $map['password'] = md5(I('password'));  //密码
            $m = M('admin');
            $result = $m->field('id,account,login_count,status,mobile,email')->where($map)->find();
            if($result){
                if($result['status'] == 0){
                    $this->error('登录失败，账号被禁用',U('Login/login'));
                }
                // $post = D('UserLoginInfo')->getcurrentPost($result['id']);
                session('username',$map['account']);   //管理员ID
                session('aid',$result['id']);   //管理员ID
                // session('rolepost',$post);   //角色岗位
                session('password',$result['password']);  //管理员密码      
                session('email',$result['email']);   //邮箱
                session('mobile',$result['mobile']);  //手机号               
                //保存登录信息
                $data['id'] = $result['id'];    //用户ID
                $data['login_ip'] = get_client_ip();    //最后登录IP
                $data['login_time'] = time();       //最后登录时间
                $data['login_count'] = $result['login_count'] + 1;
                $m->save($data);                    
                $this->success('验证成功，正在跳转到首页',U('Index/index'));            
            }else{
                $this->error('账户或密码错误',U('Login/login'));
            }
        }else{
            if(session('aid')){
                $this->error('已登录，正在跳转到主页',U('Index/index'));
            }
            $this->display('login');
        }
    }
    public function verify()
    {
        $config =    array(
            'fontSize'    =>    30,    // 验证码字体大小
            'length'      =>    4,     // 验证码位数
            'useNoise'    =>    false, // 关闭验证码杂点
        );
        $Verify =     new \Think\Verify($config);
        $Verify->entry();
    }

    /**
     * 登出
     */
    public function logout() {
        session_destroy();
        unset($_SESSION);
        $this->success('退出成功', 'login/index');
    }

}
