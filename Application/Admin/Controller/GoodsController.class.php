<?php
namespace Admin\Controller;
/**
 * 后台banner图片管理
 */
class GoodsController extends CommonController{
	/**
	 * 菜单列表
	 */
	public function index(){
		$data=D('GoodsBasicInfo')->getPageData();
		$assign=array(
			'data'=>$data['data'],
			'page'=>$data['page']
			);
		$this->assign($assign);
		$this->display('index');
	}

	/**
	 * 添加banner图片
	 */
	public function add(){
		    $data=I('post.');
        if(!empty($data)){
            if(empty($data['goods_pic'])){
              $this->error('商品图片必填');
            }    
            if($data['goods_type']==2){
              if(empty($data['goods_logo'])){
                $this->error('虚拟商品的logo必填');
              }     
            }    	
        	if(empty($data['goods_id'])){
        		//新增时，商品剩余数量等于商品数量
        		$data['goods_remain'] = $data['goods_total'];
		        $result=D('GoodsBasicInfo')->addData($data);
				if ($result) {
					$mess = array('status'=>1,'data' => $result,'url' =>U('Admin/Goods/index'));
				}else{
					$mess = array('status'=>0,'msg' => D('GoodsBasicInfo')->getError());
				}
        	}else{
        		//修改时，需要判断商品剩余数量，剩余数量会同时变动
        		$re = D('GoodsBasicInfo')->where(array('goods_id'=>$data['goods_id']))->find();
        		if(!empty($re)){
        			$goods_total = $re['goods_remain'] - ($re['goods_total'] - $data['goods_total']); 
        			if($goods_total<0){
						$this->error('商品数量不足');
        			}else{
						$data['goods_remain'] = $goods_total;
        			}
        		}else{
        			$this->error('商品未找到');
        		}
		    	$map = array('goods_id'=>$data['goods_id']);
		        $result=D('GoodsBasicInfo')->editData($map,$data);
				if ($result) {
					if($data['goods_type'] ==1 && $data['goods_spec'] == 1){
					  $mess = array('status'=>2,'data' => $result);
				    }else{
				      $mess = array('status'=>2,'url' =>U('Admin/Goods/index'));
				    }
				}else{
					$mess = array('status'=>0,'msg' => D('GoodsBasicInfo')->getError());
				}
        	}
           $this->ajaxReturn($mess);
        }else{
        	$cate = D('GoodsSpecInfo')->getAllCateName();
        	$this->assign('cate',$cate);
        	$this->display('add');
        }

	}

	/**
	 * 修改banner图片
	 */
	public function edit(){

		$id = I('get.id');
		$info = M('GoodsBasicInfo as b')
		     ->join('t_file_info as f on b.goods_pic = f.id','left')
		     ->field('b.*,f.savepath,f.savename')
		     ->where(array('b.goods_id'=>$id))
		     ->find();
		$info['goods_desc'] = preg_ueditor_image_path($info['goods_desc']);
		if(!empty($info['goods_pic'])){
		  $info['file_path']='../../../../../Uploads/Picture/'.$info['savepath'].$info['savename'];
		}
		if(!empty($info['goods_logo'])){
			$logo = M('file_info')->where(array('id'=>$info['goods_logo']))->field('savepath,savename')->find();
			$info['logo_path'] = '../../../../../Uploads/Picture/'.$logo['savepath'].$logo['savename'];
		}
		if($info['goods_type'] == 1 && $info['goods_spec'] == 1 ){
	        $setData = M('goods_spec_set_info as s')->where(array('s.goods_id'=>$id))->select();
	        foreach ($setData as $key => $value) {
		        $mapping[$key]['mapping_id']= M('goods_spec_set_detail')
		              ->where(array('set_id'=>$value['set_id']))
		              ->field('mapping_id')
		              ->select();
	        }
            $this->assign('mapping',$mapping);
        	$this->assign('list',$setData);
    		$this->assign('edit',1);			
		}
    	$cate = D('GoodsSpecInfo')->getAllCateName();
		$this->assign('info',$info);
		$this->assign('cate',$cate);
		$this->display('add');

	}

	/**
	 * 删除banner图片
	 */
	public function delete(){
		$id=I('get.id');
		$map=array(
			'goods_id'=>$id
			);
		$result=M('GoodsBasicInfo')->where($map)->delete();
		if($result){
			$this->success('删除成功',U('Admin/Goods/index'));
		}else{
			$this->error('删除失败');
		}
	}

    /**
     * *添加规格分类
     */
    public function spec_add(){
       $data = I('post.');
       $res = D('GoodsSpecInfo')->addData($data);
      if($res){
      	 $list = D('GoodsSpecInfo')->getCateName($res);
      	 $mess = array('status'=>1,'data' => $list,'pid'=>$data['pid']);
      }else{
      	 $mess = array('status'=>0,'msg'=>D('GoodsSpecInfo')->getError());
      }

      $this->ajaxReturn($mess);
    }

    public function specDel(){
    	$id = I('id');
		$map=array(
			'id'=>$id
			);
		$result=M('GoodsSpecInfo')->where($map)->delete();
		if($result){
			$this->success('删除成功');
		}else{
			$this->error('删除失败');
		}    	
    }

    //商品规则添加
    public function spec_set(){
        $data = I('post.');
    	foreach ($data as $key => $value) {
    		$code = explode('_', $key);
    		if(array_key_exists(2,$code)){
              $arr[] = $code[2];
    		}
    	}
    	$arr =array_unique($arr); 
    	$list = D('GoodsSpecSetInfo')->formData($data,$arr);
    	foreach ($list as $key => $value) {
    	     $value['goods_id'] = $data['goods_id'];
             $aid = D('GoodsSpecSetInfo')->addData($value);
             $AllId[$key] = $aid;
    	}
    	// $AllId =Array (1,2,5,6);
    	$res = D('GoodsSpecSetInfo')->addSet($data['mapping_id'],$AllId);
    	if($res){
			$this->success('添加成功',U('Admin/Goods/index'));
		}else{
			$this->error('添加失败');
		}
   
    }
  

    //商品规则修改
    public function SpecEditset(){
        $data = I('post.');
    	foreach ($data as $key => $value) {
    		$code = explode('_', $key);
    		if(array_key_exists(2,$code)){
              $arr[] = $code[2];
    		}
    	}
    	$arr =array_unique($arr); 
    	$list = D('GoodsSpecSetInfo')->formData($data,$arr);    
    	$setId = M('goods_spec_set_info')->where(array('goods_id'=>$data['goods_id']))->field('set_id')->select();
        foreach ($setId as $key => $value) {
        	$setData[] = $value['set_id'];   //获取存入数据库中的setId
        }
    	foreach ($list as $key => $value) {
    	     $value['goods_id'] = $data['goods_id']; 
    	     $value['status'] = $value['goods_status'];
             $aid = D('GoodsSpecSetInfo')->SpecData($value);
             $AllId[$key] = $aid;
             $orther[] = $value['set_id'];  //获取传值过来的setId
    	}
         $artA = array_diff($setData,$orther);  //新添加的商品分类规则
         if(!empty($artA)){
           foreach ($artA as $key => $value) {
           	   $map = array(
           	   	   'goods_id' => $data['goods_id'],
           	   	   'set_id' => $value
           	   	);
           	 $arr = D('GoodsSpecSetInfo')->where($map)->delete();
           	 $arr = D('goods_spec_set_detail')->where(array('set_id'=>$value))->delete();
           }
         }
    	$res = D('GoodsSpecSetInfo')->SpecSet($data['mapping_id'],$AllId);
     	if($arr || $res){
			$this->success('添加成功',U('Admin/Goods/index'));
		}else{
			$this->error('添加失败');
		}   	
    }
    

}
