<?php

/**
 * 获取对应状态的文字信息
 * @param int $status
 * @return string 状态文字 ，false 未获取到
 * @author huajie <banhuajie@163.com>
 */
function get_status_title($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 0  : return    '未认证';     break;
        case 1  : return    '已通过';     break;
        case 2  : return    '未通过';   break;
        case 3  : return    '审核中';   break;
        default : return    false;      break;
    }
}

function get_user_title($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        // case 0  : return    '自然人';     break;
        // case 1  : return    '机构';     break;
        case 2  : return    '战略';   break;
        case 3  : return    '核心';   break;
        case 4  : return    '营销';   break;
        default : return    false;      break;
    }
}


//中奖类型
function get_scene_title($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 0  : return    '现金';     break;
        case 1  : return    '加值券';     break;
        case 2  : return    '积分';   break;
        default : return    false;      break;
    }
}

function get_categroy_type($status = null){
    if(!isset($status)){
        return false;
    }
    $m = M('cms_category');    //查询产品类型
    $category_type = $m->where(array('category_type'=>1,'del_flag'=>'0'))->field('category_id,category_name')->select();
    foreach ($category_type as $key => $value) {
          if($value['category_id'] == $status){
             return $value['category_name'];break;
          }
    }
}

function get_constract_type($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 0  : return    '未申请';   break;
        case 1  : return    '已申请';   break;
        case 2  : return    '已邮寄';   break;
        default : return    false;      break;
    }
}

  function get_yongjin_type($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 0  : return    '未发放';   break;
        case 1  : return    '已发放';   break;
        case 2  : return    '添加佣金';   break;
        default : return    false;      break;
    }
  }
function get_product_status($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 0  : return    '未申请';   break;
        case 1  : return    '已通过';   break;
        case 3  : return    '已上线';   break;
        default : return    false;      break;
    }
}

function get_order_status($status = null){
    if(!isset($status)){
        return false;
    }
     //查询订单审核类型
    $category_type = C('ORDER_SATUTS');
    foreach ($category_type as $key => $value) {
          if($key == $status){
             return $value;break;
          }
    }
}

function get_check_status($status = null){
    if(!isset($status)){
        return false;
    }
     //查询订单审核类型
    $category_type = C('CHECK_STATUS');
    foreach ($category_type as $key => $value) {
          if($key == $status){
             return $value;break;
          }
    }
}


function get_message_status($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 1  : return    '产品';   break;
        case 2  : return    '公告/活动';   break;
        case 3  : return    '客服';   break;
        case 4  : return    '状态变更';   break;        
        default : return    false;      break;
    }
}


function get_push_type($status = null){
    if(!isset($status)){
        return false;
    }
    switch ($status){
        case 0  : return    '推送成功';   break;
        case 1  : return    '推送失败';   break;
        case 2  : return    '未推送';   break;
        default : return    false;      break;
    }
}

   //生成服务码
    function make_coupon_card() {
      $str = "0123456789"; //定义一串字符串
      $strlen = strlen($str); //获取字符串的长度(12)
      $rand = intval(rand(0,$strlen-1));  //在字符串的长度范围内随机取一个字符,字符长度不能大于字符本身的长度所以-1
      $str_a = substr($str,$rand,1); //取出字符
      //取六个随机数
      $randstr = ''; //声明一个变量
      $randarr = array(); //声明一个数组
      for($i = 1; $i < 30; $i++){
          $rand6 = intval(rand(0,$strlen-1));
          $str_a6 = substr($str,$rand6,1);
          if(!in_array($str_a6,$randarr)){ 
            $randstr .= $str_a6;
            array_push($randarr,$str_a6);
            if(count($randarr)==6){
            break;
            }
          }
       }
        return $randstr;
    }
  
    /**
     * *推送消息模板
     * @param  [type] $tempalte     添加内容
     * @param  [type] $type         消息类型
     * @param  [type] $push_type    推送用户状态 3：全部 1：单个用户
     * @param  string $userid       用户id
     * @return [type]               [description]
     */
    function push_tempalte_message($tempalte,$type,$push_type,$userid=''){
      if(empty($type) || empty($tempalte)) return false;
      $time = date('Y-m-d H:i:s',time());
      $data = array(
          'push_message_type' =>$push_type, //全部用户
          'push_template_type' =>1,
          'message_type' => $type,
          'push_time' => $time,
          'resource_id' => $userid,
          'push_result' =>2,
          'create_time' => $time
      );       
      $message = M('message_push_basic')->add($data);
      if($message){
        $tempalte['push_id'] =  $message;
        $info = M('message_tempalte')->add($tempalte);
        if($info){
          return true;
        }else{
          return false;
        }
      }
     return false;
    }



    /**********
     * 发送邮件 *
    **********/
    function SendMail($address,$title,$message)
    {
        import('Vendor.Mail.phpmailer');
        $mail=new \Verdor\Mail\PHPMailer();
        // 设置PHPMailer使用SMTP服务器发送Email
        $mail->IsSMTP();

        // 设置邮件的字符编码，若不指定，则为'UTF-8'
        $mail->CharSet='UTF-8';
        // 添加收件人地址，可以多次使用来添加多个收件人
        $address = str2arr($address);
        for ($i=0; $i <count($address) ; $i++) { 
            $mail->AddAddress($address[$i]);
        }

        // 设置邮件正文
        $mail->Body=$message;

        // 设置邮件头的From字段。
        $mail->From=C('MAIL_ADDRESS');

        // 设置发件人名字
        $mail->FromName='募易财富云';

        // 设置邮件标题
        $mail->Subject=$title;

        // 设置SMTP服务器。
        $mail->Host=C('MAIL_SMTP');

        // 设置为"需要验证"
        $mail->SMTPAuth=true;
        // $mail->addCC("xxx@qq.com");// 设置邮件抄送人，可以只写地址，上述的设置也可以只写地址(这个人也能收到邮件)  
        // $mail->addBCC("xxx@qq.com");// 设置秘密抄送人(这个人也能收到邮件)  
        // $mail->addAttachment("bug0.jpg");// 添加附件          
        //
        //$mail->IsHTML=true;
        //$mail->AltBody ="text/html";

        // 设置用户名和密码。
        $mail->Username=C('MAIL_LOGINNAME');
        $mail->Password=C('MAIL_PASSWORD');

        // 发送邮件。
        return($mail->Send());
    }

    function str2arr($str, $glue = ','){
        return explode($glue, $str);
    }

    //发送短信 
    function send_sms($mobile,$message){
        $cofig = array (
          'signName' => '募易财富云',
          'templateCode' => 'SMS_34355413' 
         );
         //发送短信
        $sms = new \Think\Sms($cofig);
        //测试模式
        $verify_code='{"name":"'.$message.'"}';//测试模板
        $status = $sms->send_verify($mobile, $verify_code);
        return $status;
    }

    /**
     * PHP发送Json对象数据
     *
     * @param $url 请求url
     * @param $jsonStr 发送的json字符串
     * @return array
     */
    function http_post_json($url, $jsonStr)
    {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json; charset=utf-8',
          'Content-Length: ' . strlen($jsonStr)
        )
      );
      $response = curl_exec($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
      if (intval ($httpCode) == 200) {
        return $response;
      } else {
        return false;
      }  
    }

/**
 * 去除多维数组中的空值
 * @author 
 * @return mixed
 * @param $arr 目标数组
 * @param array $values 去除的值  默认 去除  '',null,false,0,'0',[]
 */
function filter_array($arr, $values = ['',[]]) {
    foreach ($arr as $k => $v) {
        if (is_array($v) && count($v)>0) {
            $arr[$k] = filter_array($v, $values);
        }
        foreach ($values as $value) {
            if ($v === $value) {
                unset($arr[$k]);
                break;
            }
        }
    }
    return $arr;
}


  /**
   * 传递ueditor生成的内容获取其中图片的路径
   * @param  string $str 含有图片链接的字符串
   * @return array       匹配的图片数组
   */
  function get_ueditor_image_path($str){
      $preg='/\/Uploads\/image\/ueditor\/\d*\/\d*\.[jpg|jpeg|png|bmp|gif]*/i';
      preg_match_all($preg, $str,$data);
      return current($data);
  }

  /**
   * 将ueditor存入数据库的文章中的图片绝对路径转为相对路径
   * @param  string $content 文章内容
   * @return string          转换后的数据
   */
  function preg_ueditor_image_path($data){
      // 兼容图片路径
      $root_path=rtrim($_SERVER['SCRIPT_NAME'],'/index.php');
      // 正则替换图片
      $data=htmlspecialchars_decode($data);
      $data=preg_replace('/src=\"^\/.*\/Uploads\/image\/ueditor$/','src="'.$root_path.'/Uploads/image/ueditor',$data);
      return $data;
  }


  /**
   * 将字符串分割为数组    
   * @param  string $str 字符串
   * @return array       分割得到的数组
   */
  function mb_str_split($str){
      return preg_split('/(?<!^)(?!$)/u', $str );
  }



?>