<?php
namespace Common\Model;
use Think\Model;
use Think\Upload;

/**
 * 文件模型
 * 负责文件的下载和上传
 */

class GoodsBasicInfoModel extends Model{
     // 自动验证
    protected $_validate=array(
        array('goods_type','require','商品类型必填'), // 验证字段必填
        array('goods_name','require','商品名称必填'), // 验证字段必填
        array('goods_total','require','商品总数量必填'), // 验证字段必填
        array('goods_value', 'checkGoodstype', '虚拟商品面值必填', 0, 'callback'),     
        array('goods_condition','checkGoodsCondition','虚拟商品（满减金额）必填',0,'callback'), // 验证字段必填
        array('start_time','checkGoodsTime','券的有效开始时间必填',0,'callback'), // 验证字段必填      
        array('end_time','checkGoodsEndTime','券的有效结束时间必填',0,'callback'), // 验证字段必填  
        array('goods_desc','checkContent','商品介绍必填',0, 'callback'), // 验证字段必填                   
    );   
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s')),
        );
    function checkGoodstype(){
        $goods_type = $_POST['goods_type'];
        $goods_value = $_POST['goods_value'];
        if($goods_type==2){
           if(!empty($goods_value)){
              return true;
           }else{
              return false;
           }
        }
    }
    function checkContent(){
       $goods_desc = $_POST['goods_desc'];
       if(!empty($goods_desc)){
          return true;
       }else{
          return false;
       }
    }    
    function checkGoodsTime(){
        $goods_type = $_POST['goods_type'];
        $start_time = $_POST['start_time'];
        if($goods_type==2){
           if(!empty($start_time)){
              return true;
           }else{
              return false;
           }
        }
    }    
    function checkGoodsEndTime(){
        $goods_type = $_POST['goods_type'];
        $end_time = $_POST['end_time'];
        if($goods_type==2){
           if(!empty($end_time)){
              return true;
           }else{
              return false;
           }
        }
    }      
    function checkGoodsCondition(){
        $goods_type = $_POST['goods_type'];
        $goods_condition = $_POST['goods_condition'];
        if($goods_type==2){
           if(!empty($goods_condition)){
              return true;
           }else{
              return false;
           }
        }
    }
    /**
     * 文件上传
     * @param  array  $files   要上传的文件列表（通常是$_FILES数组）
     * @param  array  $setting 文件上传配置
     * @param  string $driver  上传驱动名称
     * @param  array  $config  上传驱动配置
     * @return array           文件上传成功后的信息
     */
    public function uploadPicture($files, $setting, $driver = 'Local', $config = null){
        /* 上传文件 */
        $Upload = new Upload($setting, $driver, $config);
        $info   = $Upload->upload($files);
        /* 设置文件保存位置 */
        $this->_auto[] = array('location', 'Ftp' === $driver ? 1 : 0, self::MODEL_INSERT);
        if($info){ //文件上传成功，记录文件信息
            foreach ($info as $key => &$value) {
                /* 记录文件信息 */
                $value['filespath'] = substr($setting['rootPath'], 1).$value['savepath'].$value['savename']; //在模板里的url路径
                if(D('FileInfo')->create($value) && ($id = D('FileInfo')->add())){
                    $value['id'] = $id;
                } 
            }
            return $info; //文件上传成功
        } else {
            $this->error = $Upload->getError();
            return false;
        }
    }

     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        // $data['goods_desc']=htmlspecialchars_decode($data['goods_desc']);
        // // 转义
        // $data['goods_desc']=htmlspecialchars($data['goods_desc']);
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }


    // 获取佣金分页数据
    public function getPageData(){
        $count = M('GoodsBasicInfo as b')
             ->join('t_file_info as f on b.goods_pic = f.id','left')
             ->count();
        $page=new \Org\Bjy\Page($count,20);
        $list= M('GoodsBasicInfo as b')
            ->join('t_file_info as f on b.goods_pic = f.id','left')
            ->field('b.*,f.savepath,f.savename')
            ->order('b.create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
            foreach ($list as $key => $value) {
               $list[$key]['file_path']='../../../Uploads/Picture/'.$value['savepath'].$value['savename'];
            }
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }


    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $count=$this
            ->where(array('pid'=>$map['id']))
            ->count();
        if($count!=0){
            return false;
        }
        $this->where(array($map))->delete();
        return true;
    }


}
