<?php
namespace Common\Model;
use Think\Model;
use Think\Upload;

/**
 * 文件模型
 * 负责文件的下载和上传
 */

class GoodsSpecSetInfoModel extends Model{
     // 自动验证
    protected $_validate=array(
        array('goods_amount','require','库存数量必填'), // 验证字段必填
        array('goods_number','require','商品货号必填'), // 验证字段必填  
    );   
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s'))
        );
     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        // print_r($data);exit;
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }


    // 获取佣金分页数据
    public function getPageData(){
        $count = M('ImageSpecInfo')->count();
        $page=new \Org\Bjy\Page($count,20);
        $list= M('ImageSpecInfo')
            ->order('create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }


    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $count=$this
            ->where(array('pid'=>$map['id']))
            ->count();
        if($count!=0){
            return false;
        }
        $this->where(array($map))->delete();
        return true;
    }

//遍历数组
    public function formData($data,$arr){
        foreach ($data as $key => $value) {
           foreach ($arr as $k => $v) {
               $code = explode('_', $key);
               $str = $code[2];
               if($str == $v){
                 $newKey = $code[0].'_'.$code[1];
                 $list[$v][$newKey]=$value;
               }
           }
        }  
        $array =filter_array($list);
        return  $array;
    }
    
   public function addSet($data,$AllId){
        if(empty($data)) return false;
        foreach ($data as $key => $value) {               
              $code = explode('_', $key);
              $id = $code[2];
              foreach ($AllId as $k => $val) {
                if($id ==$k){
                    $map = array(
                       'mapping_id'=>$value,
                       'set_id' => $val,
                       'create_time' => date('y-m-d H:i:s',time()),
                       'update_time' => date('y-m-d H:i:s',time()),
                    );
                  $res = M('goods_spec_set_detail')->add($map);
                }
              }
        }
        return $res;
   }

 
  //修改商品规格设置
  public function SpecData($data){
        if(!empty($data['set_id'])){
            $map['set_id'] = $data['set_id'];
            $res = $this->editData($map,$data);
            return $data['set_id'];
        }else{
            $aid = $this->addData($data);
            return $aid;
        }
        exit;
  }
  
  //修改商品规格分类
  public function SpecSet($data,$AllId){
    if(empty($data)) return false;
    $success = false;
    foreach ($AllId as $key => $value) {
        $arr= M('goods_spec_set_detail')->where(array('set_id'=>$value))->field('set_id,mapping_id')->select();
        if(empty($arr)){
          $Setid[$key]=$value;   //获取新增加的商品规则设置（货号表的Id）
        }else{
          $mapid[$key] = $value;  //获取修改的商品规则分类mapId
          $newMap[$key] = $arr;
        }
    }
    //将规则分类表分解为一维数组
    foreach ($newMap as $key => $value) {
        foreach ($value as $k => $val) {
          $mapping[$val['set_id'].'_'.$key].=$val['mapping_id'].','; 
          // $mapping[$key]['map_id'.$k.'_'.$key] = $val['mapping_id'];  
        }
    }
    //循环$data数组，将新增加的规则入库
    foreach ($data as $key => $value) {
          $code = explode('_', $key);
          $id = $code[2];
          foreach ($Setid as $k => $val) {
            if($id == $k){
                $map = array(
                   'mapping_id'=>$value,
                   'set_id' => $val,
                   'create_time' => date('y-m-d H:i:s',time()),
                   'update_time' => date('y-m-d H:i:s',time()),
                );
              $res = M('goods_spec_set_detail')->add($map);
              if($res){
                unset($data[$key]);  //去除新增的商品规则
              }
            }
          }
    }

     foreach ($data as $key => $value) {
        $code = explode('_', $key);
        $id = $code[2];        
        foreach ($mapid as $k => $val) {
          if($id == $k){
             $objData[$val.'_'.$id] .= $value.',';    //拼接数组，获取修改添加的值
             // $objData[$id][$key] = $value;
          }
        }
     }
     //查询新添加的数组，与弃用的商品分类规则
     foreach ($objData as $key => $value) {
       foreach ($mapping as $k => $val) {
         if($k == $key){
             $objV = array_filter(explode(',',$value));
             $mapV = array_filter(explode(',',$val));
             $artA = array_diff($objV,$mapV);  //新添加的商品分类规则
             if(!empty($artA)){
               $specA[$key] = $artA;
             }
             $artB = array_diff($mapV,$objV);  //已弃用的商品分类规则             
             if(!empty($artB)){
               $specB[$key] = $artB;
             }

         }
       }
     }
       
    //添加新增的商品分类规则
    if(!empty($specA)){
      foreach ($specA as $key => $value) {
         $set_id = explode('_', $key);
         foreach ($value as $k => $val) {
            $map = array(
              'set_id' => $set_id[0],
              'mapping_id' => $val,
              'create_time' => date('y-m-d H:i:s',time()),
              'update_time' => date('y-m-d H:i:s',time()),            
            );
            $res = M('goods_spec_set_detail')->add($map);
            if($res){
                $success= true;
            }
         }
      }      
    }
    //删除弃用的商品分类规则
    if(!empty($specB)){
      foreach ($specB as $key => $value) {
         $set_id = explode('_', $key);
         foreach ($value as $k => $val) {
            $map = array(
              'set_id' => $set_id[0],
              'mapping_id' => $val,        
            );
            $res = M('goods_spec_set_detail')->where($map)->delete();
            if($res){
                $success= true;
            }
          }
      }      
    }

    return $success;
  }
  


}
