<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 菜单操作model
 */
class AdminRoleModel extends BaseModel{

    // 自动完成
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s'))
        );
   
    //获取审批流程的相关岗位
    public function getCheckRole($type){
       $map['type'] = $type;
       $role = M('admin_role')->where($map)->order('order_number')->field('id,name,order_number')->select();
       return $role;
    }

    //获取岗位名称
    public function getRoleName($type,$order_number){
        $map = array(
           'r.type' =>$type,
           'r.order_number' =>$order_number
          );
       $rolename = M('admin_role as r')
                -> join('t_auth_group_role as g on g.role_id = r.id','left')
                -> join('t_auth_group as a on a.id = g.group_id','left') 
                ->where($map)->getField('a.title');
       return $rolename;
    }

    //获取当前岗位email
    public function getRoleEmail($type,$order_number){
        $map = array(
           'r.type' =>$type,
           'r.order_number' =>$order_number
          );
        $email = M('admin_role as r') 
            -> join('t_auth_group_role as g on g.role_id = r.id','left')
            -> join('t_auth_group_access as a on a.group_id = g.group_id','left') 
            -> join('t_admin as m on m.id = a.uid','left')
            -> where($map)
            ->getField('m.email');
       return $email;
    }    

    //获取所有岗位的邮箱
    public function getEmaillAll($type){
       $map['r.type'] = $type;
       $list = M('admin_role as r') 
            -> join('t_auth_group_role as g on g.role_id = r.id','left')
            -> join('t_auth_group_access as a on a.group_id = g.group_id','left') 
            -> join('t_admin as m on m.id = a.uid','left')
            -> where($map)
            -> field('m.account,m.email')
            -> select();
        foreach ($list as $key => $value) {
          $emails[$key] = $value['email'];
        }

        $data = implode(',',$emails);
        return $data;
    }
}
