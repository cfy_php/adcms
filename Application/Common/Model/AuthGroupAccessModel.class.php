<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 权限规则model
 */
class AuthGroupAccessModel extends BaseModel{

	/**
	 * 根据group_id获取全部用户id
	 * @param  int $group_id 用户组id
	 * @return array         用户数组
	 */
	public function getUidsByGroupId($group_id){
		$user_ids=$this
			->where(array('group_id'=>$group_id))
			->getField('uid',true);
		return $user_ids;
	}

	/**
	 * 获取管理员权限列表
	 */
	public function getAllData(){
		$data=$this
			->field('u.id,u.username,u.email,aga.group_id,ag.title')
			->alias('aga')
			->join('__USERS__ u ON aga.uid=u.id','RIGHT')
			->join('__AUTH_GROUP__ ag ON aga.group_id=ag.id','LEFT')
			->select();
		// 获取第一条数据
		$first=$data[0];
		$first['title']=array();
		$user_data[$first['id']]=$first;
		// 组合数组
		foreach ($data as $k => $v) {
			foreach ($user_data as $m => $n) {
				$uids=array_map(function($a){return $a['id'];}, $user_data);
				if (!in_array($v['id'], $uids)) {
					$v['title']=array();
					$user_data[$v['id']]=$v;
				}
			}
		}
		// 组合管理员title数组
		foreach ($user_data as $k => $v) {
			foreach ($data as $m => $n) {
				if ($n['id']==$k) {
					$user_data[$k]['title'][]=$n['title'];
				}
			}
			$user_data[$k]['title']=implode('、', $user_data[$k]['title']);
		}
		// 管理组title数组用顿号连接
		return $user_data;

	}


    public function getEmail($role){
    	 $map['_string']="FIND_IN_SET($role,g.rolepost)";
    	 // $map['g.rolepost'] = ['find_in_set',$role];
         $list = M('auth_group as g') 
                ->join('t_auth_group_access as c on g.id = c.group_id','left')
                ->join('t_admin as a on a.id = c.uid','left')
                ->where($map)
                ->field('a.id,a.account,a.email,a.mobile,g.rolepost')
                ->find();
          // echo  M('auth_group as g') ->getlastSql();exit;
         return $list;
    }

    public function getDataEmail($role){
    	 $map['_string']="FIND_IN_SET(g.rolepost,'.$role.')";
    	 // $map['g.rolePost'] = array('like','%'.$role.'%');
         $list = M('auth_group as g') 
                ->join('t_auth_group_access as c on g.id = c.group_id','left')
                ->join('t_admin as a on a.id = c.uid','left')
                ->where($map)
                ->field('a.id,a.account,a.email,a.mobile,g.rolepost')
                ->select();
      // echo  M('auth_group as g') ->getlastSql();exit;
         foreach ($list as $key => $value) {
         	$email[$key] = $value['email'];
         }
         // print_r($email);exit;
         return $email;
    }

    public function getDataMobile($role){
    	 $map['_string']="FIND_IN_SET(g.rolepost,$role)";
         $list = M('auth_group as g') 
                ->join('t_auth_group_access as c on g.id = c.group_id','left')
                ->join('t_admin as a on a.id = c.uid','left')
                ->where($map)
                ->field('a.id,a.account,a.email,a.mobile,g.rolepost')
                ->select();
 
         foreach ($list as $key => $value) {
         	$mobile[$key] = $value['mobile'];
         }
         return $mobile;
    }    
}
