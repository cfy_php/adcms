<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 ** 产品Model
 */
class UserLoginInfoModel extends BaseModel{
    // 自动验证
    protected $_validate=array(
        // array('mobile', '', '该手机号已存在', self::EXISTS_VALIDATE, 'unique'),
    );

      // 自动完成
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s')),
        array('update_time', 'date',1,'function',array('Y-m-d H:i:s'))
        );
     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return true;
        }
    }
    /**
     * 删除产品
     * @param  array $map where语句的数组形式
     * @return array      操作状态
     */
    public function deleteData($map){
        $this->where($map)->delete();
        $result=array(
            'error_code'=>0,
            'error_message'=>'操作成功'
            );
        return $result;
    }

    public function getCountData($uid,$goods_ids){
        
    }

    // 获取用户分页数据
    public function getPageData($map){
        $count=$this->where($map)->count();
        $page=new \Org\Bjy\Page($count,20);
        $list=$this
            ->where($map)
            ->order('create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        foreach ($list as $key => $value) {
            $list[$key]['more']=$this->getMorelist($value['user_id']);
        }
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }
    //相关信息
    public function getMorelist($parent_id){
       $list = M('partner_record_info as r')
              ->join('t_partner_auth_info as a on a.partner_id = r.partner_id ','left')
              ->where(array('r.partner_id'=>$parent_id))
              ->field('a.nick_name,a.id_card,a.province_code,a.city_code,a.address,a.service_code,a.resume,a.proformance,r.gender,r.age,r.email,r.work_exp')
              ->find();
       $status = C('SEX_TEXT');
       $list['gender'] = $status[$list['gender']];
       return $list;       
    }


    // 获取分页数据
    public function getPageList($map){
        $count=$this->table('t_user_login_info as u')
            ->join('t_partner_auth_info as a on a.partner_id = u.user_id  ','left')
            ->where($map)->count();
        $page=new \Org\Bjy\Page($count,20);
        $list=$this->table('t_user_login_info as u')
            ->join('t_partner_auth_info as a on a.partner_id = u.user_id','left')
            ->where($map)
            ->field('a.*,u.mobile,u.user_id,u.type')
            ->order('u.create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        foreach ($list as $key => $value) {
           $access = M('PartnerAccessInfo as a')
                  ->join('t_partner_auth_info as u on u.partner_id = a.parent_id','left')
                  ->where(array('a.user_id'=>$value['user_id']))
                  ->field('nick_name')
                  ->find();
           if(!empty($access )){
             $list[$key]['parent_name'] = $access['nick_name'];
           }
           $status = C('SEX_TEXT');
           $list[$key]['gender'] = $status[$value['gender']];
           $user_type = C('USER_TYPE');
           $list[$key]['type_text'] = $user_type[$value['type']];
           $partner_type = C('PARTNER_TYPE');
           $list[$key]['status_text'] = $partner_type[$value['status']];
        }
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;
    }
    public function getData($mobile){
        $info=$this
            ->where($mobile)
            ->field('user_id')
            ->find();
        return $info;
    }

    public function getDataByUserId($map){
      $data=$this->where($map)->find();
      return $data;
    }

    public function getUserList($map){
      $data=$this->table('t_user_login_info as u')
                 ->join('t_partner_auth_info as a on a.partner_id = u.user_id','left')
                 ->join('t_partner_record_info as r on r.partner_id = u.user_id','left')
                 ->where($map)->find();
      return $data;
    }

    public function getcurrentPost($user_id){
      $data =  M('auth_group_access as a')
              ->join('t_auth_group_role as g on g.group_id = a.group_id ','left')
              ->where(array('a.uid'=>$user_id))
              ->field('g.role_id')
              ->select();
       foreach ($data as $key => $value) {
         $rolepost[] = $value['role_id'];
       }
       $post = implode(',',$rolepost);
       return $post;       
    }
    
   public function getUserinfo($map){
      $user = $this->where($map)->field('user_id,type')->find();
      return  $user;
   }
  
}