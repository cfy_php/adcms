<?php
namespace Common\Model;
use Think\Model;
use Think\Upload;

/**
 * 文件模型
 * 负责文件的下载和上传
 */

class ImageSpecInfoModel extends Model{
     // 自动验证
    protected $_validate=array(
        array('pic_name','require','图片展示位置必填'), // 验证字段必填
        array('pic_size','require','图片长宽比例必填'), // 验证字段必填
        array('pic_ext','require','图片格式必填'), // 验证字段必填
        // array('pic_max','require', '图片最大大小必填'), // 验证字段必填
        // array('pic_color','require', '图片颜色必填'), // 验证字段必填        
    );   
    protected $_auto=array(
        array('create_time', 'date',1,'function',array('Y-m-d H:i:s'))
        );
    /**
     * 文件上传
     * @param  array  $files   要上传的文件列表（通常是$_FILES数组）
     * @param  array  $setting 文件上传配置
     * @param  string $driver  上传驱动名称
     * @param  array  $config  上传驱动配置
     * @return array           文件上传成功后的信息
     */
    public function uploadPicture($files, $setting, $driver = 'Local', $config = null){
        /* 上传文件 */
        $Upload = new Upload($setting, $driver, $config);
        $info   = $Upload->upload($files);
        /* 设置文件保存位置 */
        $this->_auto[] = array('location', 'Ftp' === $driver ? 1 : 0, self::MODEL_INSERT);
        if($info){ //文件上传成功，记录文件信息
            foreach ($info as $key => &$value) {
                /* 记录文件信息 */
                $value['filespath'] = substr($setting['rootPath'], 1).$value['savepath'].$value['savename']; //在模板里的url路径
                // $value['filespath']= substr($setting['rootPath'], 1).$value['savepath'].$value['savename']; 
                /* 记录文件信息 */
                if(D('FileInfo')->create($value) && ($id = D('FileInfo')->add())){
                    $value['id'] = $id;
                } 
            }
            // print_r($info);exit;
            return $info; //文件上传成功
        } else {
            $this->error = $Upload->getError();
            return false;
        }
    }

     /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        // print_r($data);exit;
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where($map)
                ->save($data);
            return $result;
        }
    }


    // 获取佣金分页数据
    public function getPageData(){
        $count = M('ImageSpecInfo')->count();
        $page=new \Org\Bjy\Page($count,20);
        $list= M('ImageSpecInfo')
            ->order('create_time desc')
            ->limit($page->firstRow.','.$page->listRows)
            ->select();
        $data=array(
            'data'=>$list,
            'page'=>$page->show()
            );
        return $data;

    }


    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $count=$this
            ->where(array('pid'=>$map['id']))
            ->count();
        if($count!=0){
            return false;
        }
        $this->where(array($map))->delete();
        return true;
    }


}
