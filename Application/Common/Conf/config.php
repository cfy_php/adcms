<?php
return array(
	//'配置项'=>'配置值'
	
	/* 数据库设置 */
	'DB_TYPE'           =>  'mysql',     	// 数据库类型
    'DB_HOST'           =>  'localhost',  // 服务器地址
    'DB_NAME'           =>  'thinkcms',    // 数据库名
    'DB_USER'           =>  'root',       // 用户名
    'DB_PWD'            =>  '',       // 密码

    // 'DB_HOST'           =>  '192.168.1.125',  // 服务器地址
    // 'DB_NAME'           =>  'shop',    // 数据库名
    // 'DB_USER'           =>  'cfy',      // 用户名
    // 'DB_PWD'            =>  'password',       // 密码
   
	'DB_PORT'           =>  '3306',     	// 端口
	'DB_PREFIX'         =>  't_',      	// 数据库表前缀
	'DB_DEBUG'  		=>  true, 			// 数据库调试模式 开启后可以记录SQL日志
	'SHOW_PAGE_TRACE'   =>	false,   		// 显示页面Trace信息
	
	'TMPL_ACTION_SUCCESS'=>'Public:dispatch_jump',		//自定义success跳转页面
	'TMPL_ACTION_ERROR'=>'Public:dispatch_jump',		//自定义error跳转页面

    'TMPL_PARSE_STRING'     =>  array(                        //定义常用路径
	    '__ADMIN_CSS__'     =>  __ROOT__.'/Public/Admin/static/css',
	    '__ADMIN_JS__'      =>  __ROOT__.'/Public/Admin/static/js',
	    '__ADMIN_IMAGE__'   =>  __ROOT__.'/Public/Admin/static/image',
	    '__ADMIN_OTHER__'   =>  __ROOT__.'/Public/Admin/static/other',
    ),
		

     'SYSTEM_TEXT'   =>    array(
           '1'       =>    array('name'=>'商品管理','ico'=>'list-ul','order'=>'1'),
           '2'       =>    array('name'=>'商城管理','ico'=>'list-ul','order'=>'2'),
           '3'       =>    array('name'=>'App设置','ico'=>'cogs','order'=>'3'),
           '4'       =>    array('name'=>'系统管理','ico'=>'cogs','order'=>'7'),
     ),
     'NAV_TITLE'  => array(   
         '1'       =>    '商品管理',
         '2'       =>    '商城管理',
         '3'       =>    'App设置',
         '4'       =>    '系统设置',
      ),
 

    /* 图片上传相关配置 */
    'PICTURE_UPLOAD' => array(
        'mimes'    => '', //允许上传的文件MiMe类型
        'maxSize'  => 0, //上传的文件大小限制 (0-不做限制)
        'exts'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
        'autoSub'  => true, //自动子目录保存文件
        'subName'  => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => './Uploads/Picture/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid',''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        'saveExt'  => '', //文件保存后缀，空则使用原后缀
        'replace'  => false, //存在同名是否覆盖
        'hash'     => true, //是否生成hash编码
        'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ), //图片上传相关配置（文件上传类配置）  
    'PICTURE_UPLOAD_DRIVER'=>'local',
    /* 图片上传相关配置 */
    'UPLOAD_FTP_PICTURE' => array(
        'mimes'    => '', //允许上传的文件MiMe类型
        'maxSize'  => 0, //上传的文件大小限制 (0-不做限制)
        'exts'     => 'jpg,gif,png,jpeg', //允许上传的文件后缀
        'autoSub'  => false, //自动子目录保存文件
        'subName'  => array('date', 'Y-m-d'), //子目录创建方式，[0]-函数名，[1]-参数，多个参数使用数组
        'rootPath' => '/partnerAuthPhoto/', //保存根路径
        'savePath' => '', //保存路径
        'saveName' => array('uniqid',''), //上传文件命名规则，[0]-函数名，[1]-参数，多个参数使用数组
        // 'saveExt'  => '', //文件保存后缀，空则使用原后缀
        // 'replace'  => false, //存在同名是否覆盖
        // 'hash'     => true, //是否生成hash编码
        // 'callback' => false, //检测文件是否存在回调函数，如果存在返回文件信息数组
    ), //图片上传相关配置（文件上传类配置）
    
    'PICTURE_FTP_DRIVER'=>'Ftp',
    // 'PICTURE_UPLOAD_DRIVER'=>'local',
    //本地上传文件驱动配置
    'UPLOAD_FTP_CONFIG'=>array(
        'host'     => '192.168.1.125', //服务器
        'port'     => 21, //端口
        'timeout'  => 90, //超时时间
        'username' => 'ftp', //用户名
        'password' => 'ftp', //密码
      ),
     'PICTURE_URL'=>'http://192.168.1.125',
     /*邮箱配置*/       
    'MAIL_ADDRESS'=>'appit@chenganfund.com', // 邮箱地址
    'MAIL_SMTP'=>'smtp.mxhichina.com', // 邮箱SMTP服务器
    'MAIL_LOGINNAME'=>'appit@chenganfund.com', // 邮箱登录帐号
    'MAIL_PASSWORD'=>'zaq1!@2wsx', // 邮箱密码

);
?>