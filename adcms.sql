/*
Navicat MySQL Data Transfer

Source Server         : MYSQL
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : thinkcms

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2017-07-12 10:10:21
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for t_admin
-- ----------------------------
DROP TABLE IF EXISTS `t_admin`;
CREATE TABLE `t_admin` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '管理员ID',
  `account` varchar(32) DEFAULT NULL COMMENT '管理员账号',
  `password` varchar(36) DEFAULT NULL COMMENT '管理员密码',
  `mobile` varchar(11) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(25) DEFAULT NULL COMMENT '邮箱',
  `login_time` int(11) DEFAULT NULL COMMENT '最后登录时间',
  `login_count` mediumint(8) NOT NULL COMMENT '登录次数',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '账户状态，禁用为0   启用为1',
  `create_time` int(11) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_admin_nav
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_nav`;
CREATE TABLE `t_admin_nav` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '菜单表',
  `pid` int(11) unsigned DEFAULT '0' COMMENT '所属菜单',
  `name` varchar(15) DEFAULT '' COMMENT '菜单名称',
  `mca` varchar(255) DEFAULT '' COMMENT '模块、控制器、方法',
  `ico` varchar(20) DEFAULT '' COMMENT 'font-awesome图标',
  `order_number` int(11) unsigned DEFAULT NULL COMMENT '排序',
  `type` char(1) DEFAULT NULL COMMENT '顶级栏目： 1 内容管理 2：系统设置',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_admin_role
-- ----------------------------
DROP TABLE IF EXISTS `t_admin_role`;
CREATE TABLE `t_admin_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Id',
  `name` varchar(15) DEFAULT '' COMMENT '审批岗位名称',
  `mca` varchar(255) DEFAULT '' COMMENT '描述',
  `order_number` int(11) unsigned DEFAULT NULL COMMENT '所处当前岗位顺序 1业务员 2初审 3复审 4复审二级 5终审',
  `type` char(1) DEFAULT '0' COMMENT '审批管理： 1产品 2订单 3理财师',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `t_auth_group`;
CREATE TABLE `t_auth_group` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` text NOT NULL,
  `rolepost` varchar(10) DEFAULT '0' COMMENT '当前所处岗：0普通角色，1业务员，''2''产品初审'',''3 ''产品复审一级'',''4 ''订单初审'',''5''订单复审'', ''6 ''产品复审二级'', ''7理财师初审1'', ''8理财师初审2'', ''9''总裁'',''10''理财师终审'',',
  `check_type` varchar(20) DEFAULT '0' COMMENT '审核类型： 0初始状态 1产品审核 2订单审核 3理财师审核',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `t_auth_group_access`;
CREATE TABLE `t_auth_group_access` (
  `uid` smallint(5) unsigned NOT NULL,
  `group_id` smallint(5) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_auth_group_role
-- ----------------------------
DROP TABLE IF EXISTS `t_auth_group_role`;
CREATE TABLE `t_auth_group_role` (
  `group_id` smallint(5) unsigned NOT NULL,
  `role_id` smallint(5) unsigned NOT NULL COMMENT '审批职位Id',
  UNIQUE KEY `uid_group_id` (`group_id`,`role_id`),
  KEY `role_id` (`role_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `t_auth_rule`;
CREATE TABLE `t_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则唯一标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '规则中文名称',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '' COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=248 DEFAULT CHARSET=utf8 COMMENT='规则表';

-- ----------------------------
-- Table structure for t_file_info
-- ----------------------------
DROP TABLE IF EXISTS `t_file_info`;
CREATE TABLE `t_file_info` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文件ID',
  `name` varchar(60) NOT NULL DEFAULT '' COMMENT '原始文件名',
  `savename` varchar(20) NOT NULL DEFAULT '' COMMENT '保存名称',
  `savepath` varchar(30) NOT NULL DEFAULT '' COMMENT '文件保存路径',
  `ext` varchar(5) NOT NULL DEFAULT '' COMMENT '文件后缀',
  `mime` varchar(40) NOT NULL DEFAULT '' COMMENT '文件mime类型',
  `size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `location` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '文件保存位置',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=429 DEFAULT CHARSET=utf8 COMMENT='文件表';

-- ----------------------------
-- Table structure for t_goods_basic_info
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_basic_info`;
CREATE TABLE `t_goods_basic_info` (
  `goods_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品ID',
  `goods_type` varchar(2) DEFAULT '1' COMMENT '商品类型（1实物类，2虚拟类）',
  `goods_name` varchar(20) DEFAULT NULL COMMENT '商品名称',
  `goods_brand` varchar(10) DEFAULT NULL COMMENT '商品品牌',
  `goods_pic` varchar(36) DEFAULT NULL COMMENT '商品图片',
  `goods_spec` varchar(1) DEFAULT '0' COMMENT '规格 0：无 1：有',
  `goods_total` int(255) DEFAULT '0' COMMENT '商品总数量',
  `goods_value` varchar(255) DEFAULT NULL COMMENT '虚拟商品面值',
  `goods_condition` varchar(20) DEFAULT NULL COMMENT '虚拟商品使用条件（满减金额）',
  `goods_logo` varchar(36) DEFAULT NULL COMMENT '虚拟商品的logo',
  `start_time` varchar(20) DEFAULT NULL COMMENT '虚拟商品开始有效时间',
  `end_time` varchar(20) DEFAULT NULL COMMENT '虚拟商品结束有效时间',
  `goods_memo` mediumtext COMMENT '商品备注',
  `goods_desc` mediumtext COMMENT '商品介绍',
  `update_time` timestamp NOT NULL COMMENT '更新时间',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for t_goods_spec_info
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_spec_info`;
CREATE TABLE `t_goods_spec_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品规格ID',
  `pid` int(11) DEFAULT '0' COMMENT '所属规格',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `update_time` timestamp NOT NULL COMMENT '更新时间',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COMMENT='规格分类字典表';

-- ----------------------------
-- Table structure for t_goods_spec_set_detail
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_spec_set_detail`;
CREATE TABLE `t_goods_spec_set_detail` (
  `set_detail_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品规格设置详情ID',
  `set_id` int(36) DEFAULT NULL COMMENT '商品规格设置ID',
  `mapping_id` int(50) DEFAULT NULL COMMENT '商品规格映射ID',
  `update_time` timestamp NOT NULL COMMENT '更新时间',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`set_detail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='商品规格设置详细信息';

-- ----------------------------
-- Table structure for t_goods_spec_set_info
-- ----------------------------
DROP TABLE IF EXISTS `t_goods_spec_set_info`;
CREATE TABLE `t_goods_spec_set_info` (
  `goods_id` int(11) DEFAULT NULL COMMENT '商品ID',
  `set_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品规格设置ID',
  `goods_amount` varchar(36) DEFAULT NULL COMMENT '数量',
  `goods_number` varchar(36) DEFAULT NULL COMMENT '货号',
  `status` varchar(1) DEFAULT NULL COMMENT '状态 0：启用 1：禁用',
  `update_time` timestamp NOT NULL COMMENT '更新时间',
  `create_time` timestamp NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`set_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='商品规格表';

-- ----------------------------
-- Table structure for t_image_spec_info
-- ----------------------------
DROP TABLE IF EXISTS `t_image_spec_info`;
CREATE TABLE `t_image_spec_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pic_name` varchar(20) DEFAULT NULL COMMENT '展示位置',
  `pic_size` varchar(36) DEFAULT NULL COMMENT '长宽比例',
  `pic_ext` varchar(10) DEFAULT NULL COMMENT '图片格式',
  `pic_max` varchar(20) DEFAULT NULL COMMENT '图片最大大小',
  `pic_color` varchar(10) DEFAULT NULL COMMENT '图片颜色',
  `create_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='图片规格表';
